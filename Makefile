#
## Makefile for building and publishing the `dspsr` image.
#
#
## ----------------------------------------------------------------------------
# Variables
# # ----------------------------------------------------------------------------
IMAGE_NAME:=dspsr
VERSION:=$(shell awk -F= '/.*/{print $$1}' VERSION)

#
# ----------------------------------------------------------------------------
#  # Docker helper targets
# ----------------------------------------------------------------------------
#
.PHONY: build
build:  ## Build the docker image
	docker build -t $(IMAGE_NAME):$(VERSION) .

# Run make build to build a local version of the dspsr image

