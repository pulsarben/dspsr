##############################################################################################
## Purpose   : Dockerize Components of Pulsar software stack.
## Author    : Lina Levin (lina.preston@manchester.ac.uk)
## Author    : Benjamin Shaw (benjamin.shaw@manchester.ac.uk)
###############################################################################################
##
## This docker file will setup an environment for DSPSR and its dependencies
##
## SOFTWARE:
##
## 1. TEMPO
## 2. TEMPO2
## 3. PSRCHIVE
## 4. DSPSR (PENDING)
###############################################################################################

# Select base OS
FROM centos:7

LABEL authors="Levin L, Shaw B"

# WORKDIR sets the location in the container 
# where subsequent commands are run
WORKDIR /home

# Next we'll install dependencies that are
# available from existing yum repositories 
# ===================================
# Install OS software
# ===================================
# tcsh. C-shell
# unzip. For unzipping zip archives
# wget. File retrievals over ftp or http
# ===================================
# Install dev tools
# ===================================
# make. Control software compilation process
# autoconf. Configures source codes and makefiles
# automake. Automatically generates makefiles.
# gcc. GNU compiler for compiling C
# gcc-fortran. GNU compiler for Fortran
# ===================================
# Install packages required by TEMPO2
# ===================================
# libtool. GNU Libtool Dynamic Module Loader
# gcc-c++. Adds c++ support to the GNU compiler
RUN yum -y install epel-release  && \
    yum -y update && yum -y install \
    tcsh \ 
    unzip \ 
    wget \ 
    make \ 
    autoconf \
    automake \ 
    gcc \
    gcc-gfortran \
    libtool \
    gcc-c++ \
    cfitsio \
    cfitsio-devel \
    fftw-devel \
    git \ 
    patch \
    eog \
    evince \ 
    xterm  \
    swig

RUN yum -y install \
    python-pip \
    python-devel  && \
    pip install --no-cache-dir --upgrade pip  && \
    pip install --no-cache-dir pip -U  && \
    pip install --no-cache-dir setuptools -U  && \
    pip install --no-cache-dir numpy -U && \
    pip install --no-cache-dir scipy -U  && \
    pip install --no-cache-dir matplotlib -U && \
    pip install --no-cache-dir pyephem -U

# =========================================
# Install pgplot
# ========================================
COPY pgplot/rpmfusion-nonfree-release-7.noarch.rpm pgplot/rpmfusion-free-release-7.noarch.rpm /tmp/
WORKDIR /tmp
RUN rpm -Uvh rpmfusion-free-release-7.noarch.rpm && \
    rpm -Uvh rpmfusion-nonfree-release-7.noarch.rpm && \
    yum -y install pgplot-devel

# ==============================================
# Set up environment
# =============================================

# Set location of software
RUN mkdir -p /home/psr/soft

# Define home, psrhome, OSTYPE
ENV HOME=/home
ENV PSRHOME=/home/psr/soft
ENV OSTYPE=linux

ENV SIGPROC=$PSRHOME/sigproc
ENV PATH=$PATH:$SIGPROC/install/bin
ENV FC=gfortran
ENV F77=gfortran
ENV CC=gcc
ENV CXX=g++
# ============================================
# Download software
# ============================================

# Get TEMPO and TEMPO2, unzip them and move them to the
# right locations

WORKDIR /home
COPY tempo/Tempo_SNAPSHOT_08_12_2016.zip tempo2/Tempo2_2016.11.3_SNAPSHOT_08_12_2016.zip /home/
#RUN wget https://github.com/scienceguyrob/SKA-TestVectorGenerationPipeline/raw/master/Deploy/Software/08_12_2016/Tempo_SNAPSHOT_08_12_2016.zip && \
#    wget https://github.com/scienceguyrob/SKA-TestVectorGenerationPipeline/raw/master/Deploy/Software/08_12_2016/Tempo2_2016.11.3_SNAPSHOT_08_12_2016.zip && \
RUN unzip Tempo2_2016.11.3_SNAPSHOT_08_12_2016.zip -d /home/Tempo2_2016.11.3_SNAPSHOT_08_12_2016 && \
    unzip Tempo_SNAPSHOT_08_12_2016.zip -d /home/Tempo_SNAPSHOT_08_12_2016 && \
    rm Tempo_SNAPSHOT_08_12_2016.zip && \
    rm Tempo2_2016.11.3_SNAPSHOT_08_12_2016.zip && \
    mv /home/Tempo_SNAPSHOT_08_12_2016 /home/psr/soft/tempo && \
    mv /home/Tempo2_2016.11.3_SNAPSHOT_08_12_2016 /home/psr/soft/tempo2

# =============================================
# Install TEMPO
# =============================================

ENV TEMPO=$PSRHOME/tempo
ENV PATH=$PATH:$PSRHOME/tempo/bin

WORKDIR $PSRHOME/tempo
RUN ./prepare && \
    ./configure --prefix=$PSRHOME/tempo && \
    make && \
    make install && \
    mv obsys.dat obsys.dat_ORIGINAL 
COPY tempo/obsys.dat $PSRHOME/tempo

# ============================================
# Install TEMPO2
# ============================================

ENV TEMPO2=$PSRHOME/tempo2/T2runtime
ENV PATH=$PATH:$PSRHOME/tempo2/T2runtime/bin
ENV C_INCLUDE_PATH=$C_INCLUDE_PATH:$PSRHOME/tempo2/T2runtime/include
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PSRHOME/tempo2/T2runtime/lib
#
WORKDIR $PSRHOME/tempo2
RUN ./bootstrap && \
    ./configure --x-libraries=/usr/lib/x86_64-linux-gnu --enable-shared --enable-static --with-pic F77=gfortran && \
    make && \
    make install && \
    make plugins-install
WORKDIR $PSRHOME/tempo2/T2runtime/observatory
RUN mv observatories.dat observatories.dat_ORIGINAL && \
    mv oldcodes.dat oldcodes.dat_ORIGINAL && \
    mv aliases aliases_ORIGINAL
COPY tempo2/observatories.dat tempo2/aliases $PSRHOME/tempo2/T2runtime/observatory/

# ============================================
# Install PSRCHIVE
# ============================================

ENV PSRCAT_FILE=$PSRHOME/psrcat_tar/psrcat.db
ENV PATH=$PATH:$PSRHOME/psrcat_tar

ENV PATH=$PATH:$PSRHOME/bin
#ENV PGPLOT_DIR=$PSRHOME/pgplot
#ENV PGPLOT_FONT=$PGPLOT_DIR/grfont.dat

WORKDIR $PSRHOME
#RUN git clone git://git.code.sf.net/p/psrchive/code psrchive
RUN git clone https://pulsarben@bitbucket.org/pulsarben/psrchive.git
WORKDIR $PSRHOME/psrchive
RUN ./bootstrap  && \
    ./configure --enable-shared || exit 0
WORKDIR  $PSRHOME/psrchive/packages
RUN make 
WORKDIR  $PSRHOME/psrchive/
# CFITSIO doesn't install yet
RUN ./packages/epsic.csh  && \
    ./configure --enable-shared  && \ 
    make && \ 
    make check && \
    make install 

# ============================================
# Install dspsr
# ===========================================

WORKDIR $PSRHOME
RUN git clone git://git.code.sf.net/p/dspsr/code dspsr
WORKDIR $PSRHOME/dspsr
# I've no idea whether this backends list of correct/what we want..
# RUN echo "apsr bpsr cpsr2 caspsr mopsr sigproc" > backends.list && \ 
RUN echo "apsr asp bcpm bpsr cpsr cpsr2 gmrt lump lbadr lbadr64 mark4 mark5 maxim mwa pdev pmdaq puma2 spda1k guppi sigproc vdif lwa kat" > backends.list && \
    ./bootstrap && \ 
    ./configure && \ 
    make && \ 
    make install
# This compiles fine so far....
# I've tested pav with some archive files
# and I'm able to write a ps file to disk 
# using -g filename.ps/ps. (but not png)
# I am also also to fold and dedisperse
# a filterbank file using dspsr.

ENV PYTHONPATH "${PYTHONPATH}:$PSRHOME/lib/python2.7/site-packages"

# This command will be executed
# when docker starts the container
WORKDIR /home
ENTRYPOINT /bin/bash
